package Myprojects.Vinothproject;

public class QueueArrayAlternate {

	int capacity;
	int queArray[];
	int front;
	int rear;
	int currentSize;

	public QueueArrayAlternate(int queueSize) {
		this.capacity=queueSize;
		this.front=0;
		this.rear=-1;
		queArray=new int[this.capacity];
	}

	public boolean isEmpty() {
		if(rear==-1) {
			return true;
		}else {
			return false;
		}
	}

	public boolean isFull() {
		if(rear==capacity-1) {
			return true;
		}else
		{
			return false;
		}
	}

	public void enqueue(int value) {
		if(isFull()) {
			System.out.println("the queue is full");
		}
		else
		{
			if(rear==capacity-1) {
				rear=-1;
			}
			queArray[++rear]=value;
			System.out.println("the value added is "+value);
			currentSize++;
		}
	}


	public void dequeue() {
		if(isEmpty()) {
			System.out.println("The value is empty");
		}
		else
		{
			System.out.println("Removing the element "+queArray[front++]);
			if(front==capacity) {
				front =0;
			}
			currentSize--;
			
			
//			if((rear>=0)) {
//			front++;
//			System.out.println("the Removed value is "+queArray[front-1]);
//			rear--;
//		}
		}
	}


	public static void main(String[] args) {
		QueueArray queue=new QueueArray(5);
		queue.enqueue(10);
		queue.enqueue(20);
		queue.enqueue(30);
		queue.enqueue(40);
		queue.enqueue(50);
		queue.enqueue(70);
		queue.dequeue();
		queue.dequeue();
		queue.dequeue();
		queue.dequeue();
		queue.dequeue();
		
//		queue.enqueue(10);
//		queue.enqueue(20);
//		queue.enqueue(30);
//		queue.dequeue();
//		queue.enqueue(40);
//		queue.dequeue();
//		queue.enqueue(50);
//		queue.enqueue(60);
//		queue.dequeue();
//		queue.dequeue();
		
	}

}
