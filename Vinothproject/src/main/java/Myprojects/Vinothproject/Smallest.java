package Myprojects.Vinothproject;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;

public class Smallest {

	public static void main(String[] args) {
		
		String[] values= {"a","1","15","p","21","17","k","99","g"};
		List<Integer> numberList=new ArrayList<>();
		for(int i=0 ; i<values.length;i++) {
			int number=isNumber(values[i]);
			if(!(number==-1)) {
				numberList.add(number);
			}
		}
		System.out.println("count " +numberList.stream().sorted().count());
		
		Integer[] intarray=new Integer[numberList.size()];
		numberList.toArray(intarray);
		
		for(int a:numberList ) {
			System.out.println(a);
		}
		
		for(int i=0; i<intarray.length; i++) {
			for(int j=0 ; j<intarray.length-1;j++) {
				if(intarray[j]>intarray[j+1]) {
					int temp=intarray[j];
					intarray[j]=intarray[j+1];
					intarray[j+1]=temp;
				}
			}
		}
		
		
		
		for(int y: intarray) {
			System.out.println(y);
		}
		
		
		System.out.println(intarray[0]+intarray[1]);
		
	}
	
	public static int isNumber(String value) {
		try {
		int number=Integer.parseInt(value);
		return number;
		}
		catch(NumberFormatException ne) {
			
			return -1;
		}
		
	}

}
