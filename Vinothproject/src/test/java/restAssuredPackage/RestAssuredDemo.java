package restAssuredPackage;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RestAssuredDemo {

	
	public static void main(String[] args) {
		
		Response response=RestAssured.given().get("https://mocki.io/v1/59ae57d9-e28c-46f7-8883-00a0013dbc20");
		System.out.println(response.asString());
		System.out.println(response.asPrettyString());
		String jsonString= response.asString(); 
		Map<String,Object> responseMap=(Map<String, Object>) RestAssured.given().
				get("https://mocki.io/v1/59ae57d9-e28c-46f7-8883-00a0013dbc20").as(new TypeRef<HashMap<String,Object>>(){});
		JsonPath jsonPath=new JsonPath(jsonString);
		JsonPath jsonPath1=response.jsonPath();
		System.out.println((Object)jsonPath.get("$"));
		System.out.println((Object)jsonPath.get());
		Object salary=jsonPath1.get("employee.name");
		
		if(salary instanceof Integer)
		{
			System.out.println("obj salary is a integer"+salary);	
		}
		else
		if(salary instanceof String)
		 {
			System.out.println("obj salary is a String" +salary);
		}
		
		
		
		
		Map<String,Object> employeeMap=(Map<String, Object>) responseMap.get("employee");
		System.out.println("Line 34 " +employeeMap.get("name"));
	//	System.out.println(String.valueOf(employeeMap.get("salary")));
		System.out.println(employeeMap.get("salary"));
		System.out.println(employeeMap.get("id"));
		Object o=employeeMap.get("id");
		if(o instanceof Integer) {
			System.out.println("Integer value returned");
		}
		else {
			System.out.println("Not an String");
		}
		
		Object o1=employeeMap.get("name");
		if(o1 instanceof Integer) {
			System.out.println("Integer value returned");
		}
		else {
			System.out.println("not an integer");
		}
		
		}
	}
	


