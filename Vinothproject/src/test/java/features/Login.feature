Feature: Login functionality

  Scenario Outline: Check login is successfull with correct credentials
    Given User is on login page
    When user enters "<username>" and "<password>"
    And clicks on login button
    Then User is navigated to home page

		Examples:
			|username  | password |
			|vinoth		 | jankin   |
			|vidhya		 | gherkin  |