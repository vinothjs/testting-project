package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStepDefinition {

	@Given("User is on login page")
	public void user_is_on_login_page() {
		System.out.println("Printing User is on login page");
		
	}

	@When("user enters {string} and {string}")
	public void user_enters_and(String userName, String password) {
		System.out.println("enter "+userName +" and " +password);
	}

	@When("clicks on login button")
	public void clicks_on_login_button() {
		System.out.println("Printing clicks on login button");
	}

	@Then("User is navigated to home page")
	public void user_is_navigated_to_home_page() {
		System.out.println("Printing User is navigated to home page");
	}
	
}
