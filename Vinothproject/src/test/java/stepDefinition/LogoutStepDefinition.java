package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LogoutStepDefinition {

	@Given("User is in home page")
	public void user_is_in_home_page() {
	   System.out.println("Printing User is in home page ");
	}

	@When("User clicks on logout button")
	public void user_clicks_on_logout_button() {
		System.out.println("User clicks on logout button ");
	}

	@Then("User gets Logged out")
	public void user_gets_logged_out() {
		System.out.println("Printing User gets Logged out");
	}
	
}
